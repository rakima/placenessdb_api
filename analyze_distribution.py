# -*- coding: utf-8 -*-

from src import dbhandler
from src import cognitiveAPI
from src.jsonencoder import *
import numpy as np
import subprocess
import cv2
import os  
import urllib
import json
from bs4 import BeautifulSoup
from flask import Flask
from flask import request
from src import dbhandler
import sys 

dat_db = dbhandler.firebase('https://placenessdb2.firebaseio.com/experiment/')
posts = dat_db.get('/coex/') 

total_dict = {} 

for post in posts: 
    dat = posts[post]
    
    if 'text_keywords' in dat.keys():
        #print dat['text_keywords'] 
        for activity in dat['text_keywords']:
            weekday = dat['time_keywords']['weekday']
            isWeekend="isWeekend"
            if weekday=="Saturday" or weekday=="Sunday":
                isWeekend="isWeekday"
            timeofday = dat['time_keywords']['timeofday']
            season = dat['time_keywords']['season']
            buf_key = activity+"-"+isWeekend+"-"+timeofday
            if not buf_key in total_dict.keys():
                total_dict[buf_key] = 1
            else:
                total_dict[buf_key] += 1
                
print total_dict

f = open("results2.txt", "w");

for key in total_dict:
    print key + ", " + str(total_dict[key])
    f.write(key + ", " + str(total_dict[key]) + '\n')
f.close()
            

    